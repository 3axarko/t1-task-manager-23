package ru.t1.zkovalenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.repository.IUserRepository;
import ru.t1.zkovalenko.tm.enumerated.Role;
import ru.t1.zkovalenko.tm.model.User;
import ru.t1.zkovalenko.tm.util.HashUtil;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password) {
        @NotNull final User user = new User();
        user.setLogin(login);
        final String saltPass = HashUtil.salt(password);
        user.setPasswordHash(saltPass == null ? "" : saltPass);
        user.setRole(Role.USUAL);
        return add(user);
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password, @NotNull final String email) {
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password, @Nullable final Role role) {
        @NotNull final User user = create(login, password);
        if (role != null) user.setRole(role);
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return models.stream()
                .filter(m -> login.equals(m.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        return models.stream()
                .filter(m -> email.equals(m.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        return models.stream()
                .anyMatch(m -> login.equals(m.getLogin()));
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull final String email) {
        return models.stream()
                .anyMatch(m -> email.equals(m.getEmail()));
    }

}
