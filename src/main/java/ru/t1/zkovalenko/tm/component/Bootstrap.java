package ru.t1.zkovalenko.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.repository.ICommandRepository;
import ru.t1.zkovalenko.tm.api.repository.IProjectRepository;
import ru.t1.zkovalenko.tm.api.repository.ITaskRepository;
import ru.t1.zkovalenko.tm.api.repository.IUserRepository;
import ru.t1.zkovalenko.tm.api.service.*;
import ru.t1.zkovalenko.tm.command.AbstractCommand;
import ru.t1.zkovalenko.tm.command.project.*;
import ru.t1.zkovalenko.tm.command.system.*;
import ru.t1.zkovalenko.tm.command.task.*;
import ru.t1.zkovalenko.tm.command.user.*;
import ru.t1.zkovalenko.tm.enumerated.Role;
import ru.t1.zkovalenko.tm.enumerated.Status;
import ru.t1.zkovalenko.tm.exception.system.CommandNotSupportedException;
import ru.t1.zkovalenko.tm.model.Project;
import ru.t1.zkovalenko.tm.model.User;
import ru.t1.zkovalenko.tm.repository.CommandRepository;
import ru.t1.zkovalenko.tm.repository.ProjectRepository;
import ru.t1.zkovalenko.tm.repository.TaskRepository;
import ru.t1.zkovalenko.tm.repository.UserRepository;
import ru.t1.zkovalenko.tm.service.*;
import ru.t1.zkovalenko.tm.util.ColorizeConsoleTextUtil;
import ru.t1.zkovalenko.tm.util.TerminalUtil;

@Getter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository);

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    {
        registry(new ApplicationAboutCommand());
        registry(new SystemInfoCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());
        registry(new ApplicationExitCommand());

        registry(new ProjectListCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByIdCommand());

        registry(new TaskListCommand());
        registry(new TaskCreateCommand());
        registry(new TaskClearCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskShowByProjectIdCommand());

        registry(new UserRegistryCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserViewProfileCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserLockCommand());
        registry(new UserUnlockCommand());
        registry(new UserRemoveCommand());
    }

    private void processArgument(@Nullable final String argument) {
        if (argument == null) throw new CommandNotSupportedException();
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new CommandNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void processCommand(@Nullable final String command) {
        if (command == null) throw new CommandNotSupportedException();
        @Nullable AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) abstractCommand = commandService.getCommandByArgument(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void exit() {
        System.exit(0);
    }

    public void run(@Nullable String[] args) {
        if (processArguments(args)) exit();

        initDemoData();
        initLogger();

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("Enter Command:");
                @NotNull final String command = TerminalUtil.nextLine();
                System.out.println("---");
                processCommand(command);
                ColorizeConsoleTextUtil.greenText("[OK]");
                loggerService.command(command);
            } catch (@Nullable final Exception e) {
                ColorizeConsoleTextUtil.redText("[FAIL]");
                loggerService.error(e);
            }
        }
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private boolean processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private void initLogger() {
        loggerService.info("** Welcome to Task-Manager **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                loggerService.info("** Task-Manager is shutting down **");
            }
        });
    }

    private void initDemoData() {
        final User firstUser = userService.create("1", "1", "12@mail.ru");
        userService.create("2", "2", "13@mail.ru");
        userService.create("admin", "admin", Role.ADMIN);

        projectService.add(firstUser.getId(), new Project("project1", Status.IN_PROGRESS));
        projectService.add(firstUser.getId(), new Project("project3", Status.NOT_STARTED));
        projectService.add(firstUser.getId(), new Project("project2", Status.IN_PROGRESS));
        projectService.add(firstUser.getId(), new Project("project4", Status.COMPLETED));

        taskService.create(firstUser.getId(), "task1");
        taskService.create(firstUser.getId(), "task2");

//        getAuthService().login("1", "1");
//        getProjectTaskService().removeProjectByIndex(firstUser.getId(), 2);
    }

}
