package ru.t1.zkovalenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.zkovalenko.tm.api.service.IAuthService;
import ru.t1.zkovalenko.tm.api.service.IUserService;
import ru.t1.zkovalenko.tm.enumerated.Role;
import ru.t1.zkovalenko.tm.exception.field.LoginEmptyException;
import ru.t1.zkovalenko.tm.exception.field.PasswordEmptyException;
import ru.t1.zkovalenko.tm.exception.user.AccessDeniedException;
import ru.t1.zkovalenko.tm.exception.user.IncorrectLoginOrPasswordException;
import ru.t1.zkovalenko.tm.exception.user.PermissionException;
import ru.t1.zkovalenko.tm.model.User;
import ru.t1.zkovalenko.tm.util.HashUtil;

import java.util.Arrays;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull IUserService userService) {
        this.userService = userService;
    }

    @NotNull
    @Override
    public User registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        return userService.create(login, password, email);
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new IncorrectLoginOrPasswordException();
        if (user.getLocked()) throw new IncorrectLoginOrPasswordException();
        final String hash = HashUtil.salt(password);
        if (hash != null && !hash.equals(user.getPasswordHash())) throw new IncorrectLoginOrPasswordException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @NotNull
    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @NotNull
    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        return userService.findOneById(userId);
    }

    @Override
    public void checkRoles(@Nullable final Role[] roles) {
        if (roles == null) return;
        final User user = getUser();
        final Role role = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

}
