package ru.t1.zkovalenko.tm.command.system;

import org.jetbrains.annotations.NotNull;

public class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    public static final String DESCRIPTION = "Who did it? And why?";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Who is the best: Read below");
        System.out.println("Developer: 3axarko");
        System.out.println("And he's really good");
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
